#include "Afd.h"

Afd::Afd(std::string fileName, std::vector<std::string>& Q, std::vector<char>& Z, std::string& punctStart, std::vector<std::string>& finale, std::vector<std::vector<std::string>>& F)
{
	citire(fileName, Q, Z, punctStart, finale, F);
	for (int i = 0; i < Q.size(); i++)
		m_Q.push_back(Q[i]);

	for (int i = 0; i < Z.size(); i++)
		m_Z.push_back(Z[i]);

	m_punctStart = punctStart;

	for (int i = 0; i < finale.size(); i++)
		m_finale.push_back(finale[i]);

	for (int i = 0; i < Q.size()*Z.size(); i++)
	{
		m_F.push_back(F[i]);
	}
	

	
}



void Afd::citire(std::string fileName, std::vector<std::string>& Q, std::vector<char>& Z, std::string &punctStart, std::vector<std::string>& finale, std::vector<std::vector<std::string>>& F)
{
	int line_number = 0;
	std::ifstream file(fileName);

	if (file.is_open())
	{
		std::string line;
		while (std::getline(file, line))
		{
			line_number++;
			char c = ' ';
			std::string s = "";
			if (line_number == 1)
			{
				for (int i = 0; i < line.size(); i++)
				{
					if (line[i] != ' ')
					{
						s = s + line[i];
					}
					else
					{
						Q.push_back(s);
						s = "";
					}

				}
				Q.push_back(s);
			}

			if (line_number == 2)
			{
				for (int i = 0; i < line.size(); i++)
				{
					if (line[i] != ' ')
						Z.push_back(line[i]);

				}
			}

			if (line_number == 3)
			{
				punctStart = line;

			}

			if (line_number == 4)
			{
				s = "";
				for (int i = 0; i < line.size(); i++)
				{
					if (line[i] != ' ')
					{
						s = s + line[i];
					}
					else
					{
						finale.push_back(s);
						s = "";
					}

				}
				finale.push_back(s);

				std::vector<std::string> aux;
				int nr_relatii = Q.size() * Z.size();
				int pas = 0;
				std::string a;
				std::string b;
				std::string c;
				while (pas <= nr_relatii)
				{

					file >> a;
					file >> b;
					file >> c;
					aux.push_back(a);
					aux.push_back(b);
					aux.push_back(c);
					F.push_back(aux);
					aux.clear();
					pas++;

				}
			}


			


		}
		file.close();

	}
}

void Afd::afisare()
{
	std::cout << "Q = { ";
	for (int i = 0; i < m_Q.size(); i++)
	{
		if (i != m_Q.size() - 1)
			std::cout << m_Q[i] << ", ";
		else
			std::cout << m_Q[i] << " ";
	}
		
	std::cout <<"}"<< std::endl;

	std::cout << "Z = { ";
	for (int i = 0; i < m_Z.size(); i++)
	{
		if (i != m_Z.size() - 1)
			std::cout << m_Z[i] << ", ";
		else
			std::cout << m_Z[i] << " ";
	}
	std::cout << "}" << std::endl;
	
	std::cout << "Punct de start = " << m_punctStart << "\n";

	std::cout << "Puncte finale = { ";
	for (int i = 0; i < m_finale.size(); i++)
	{
		if (i != m_finale.size() - 1)
			std::cout << m_finale[i] << ", ";
		else
			std::cout << m_finale[i] << " ";
	}

	std::cout << "}" << std::endl;


	for (int i = 0; i < m_Q.size() * m_Z.size(); i++)
	{
		
		std::cout << "d( " << m_F[i][0] << ", " << m_F[i][1] << " ) = " << m_F[i][2];
		std::cout << "\n";
	}
}

void Afd::verificare(std::string& cuvant, std::string& start)
{
	int ok = 0;
	std::string c;
	c = cuvant.at(0);
	std::string newStart;
	for (int i = 0; i < m_Q.size() * m_Z.size(); i++)
	{
		if (m_F[i][0] == start && m_F[i][1] == c)
		{
			ok = 1;
			std::cout << "d( " << start << ", " << c << " ) = " << m_F[i][2] << "\n";
			newStart = m_F[i][2];
			
			
		}
	}
		
	if (ok == 1)
	{
		cuvant.erase(cuvant.begin());
		start = newStart;
		

		if (cuvant.length() == 0)
		{
			int gasit_final = 0;
			for (int i = 0; i < m_finale.size(); i++)
				if (start == m_finale[i])
					gasit_final = 1;

			if (start == "#")
				std::cout << "BLOCAJ" << std::endl;
			else if (gasit_final == 0)
			{
				std::cout << "NEACCEPTAT!" << std::endl;
			}
			else
				std::cout << "ACCEPTAT!" << std::endl;
		}

		else
		{
			if (start == "#")
				std::cout << "BLOCAJ" << "\n";
			else
				verificare(cuvant, start);
		}
	}

			
	
	if (ok == 0)
	{
		std::cout << "BLOCAJ!" << std::endl;
	}
		
	
}
