#pragma once
#include <iostream>
#include <vector>
#include <string>
#include <fstream>


class Afd
{
private:
	std::vector<std::string> m_Q;
	std::vector<int> m_Vector;
	std::vector<char> m_Z;
	std::string m_punctStart;
	std::vector<std::string> m_finale;
	std::vector<std::vector<std::string>> m_F;
	

public:
	int steps = 0;
	Afd(std::string fileName, std::vector<std::string>& Q, std::vector<char>& Z, std::string& punctStart, 
		std::vector<std::string>& finale, std::vector<std::vector<std::string>>& F);
	
	void citire(std::string fileName, std::vector<std::string>& Q, std::vector<char>& Z, std::string& punctStart, std::vector<std::string>& finale, std::vector<std::vector<std::string>>& F);
	
	void afisare();

	void verificare(std::string& cuvant, std::string& start);
};

